#README

## PIBIC

Programa pesquisa para o SUS.

Projetos recebidos para avaliação.

### Função desse repositório ###

* Armazenamento em nuvem
* Versionamento de dados dos projetos
* Testar tecnologias de automação para os documentos

### Autoria e responsabilidade ###

* Francisco H C Felix, Pediatra Cancerologista
* Centro Pediátrico do Câncer - Hospital Infantil Albert Sabin
